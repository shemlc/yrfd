/*
Change the variables here for the next talk.
Also check the
	- meeting link in index.html
	- adjust the date and time in times.html for required timezones
Insert linebreaks with <br>
*/

var next_talk_URL = "https://ucf.zoom.us/skype/99936060606"

var next_talk_time_UTC_date = "May 15th (Monday), 2023"
var next_talk_time_UTC_hour = 13 /*Use 24 hour time, no quotes*/
var next_talk_time_UTC_minute = "30"

var next_talk_season_number = "3";
var next_talk_episode_number = "13";

var next_talk_title = "Exploring the Mechanism Driving Asymmetry of Imploding Detonations in Thin Channels"; /*no season or episode number*/

var next_talk_name = "Sebastian Rodriguez Rosero";

var next_talk_position ="Research Assistant"

var next_talk_institute = "McGill University"

var next_talk_country = "Canada"

var next_talk_abstract = "Due to the potential to attain high-energy-density states with minimal energy input, imploding detonations remain a fascinating phenomenon. Their known applications include serving as effective pre-detonators for fuel-air engines and as initiators for fusion energy reactors. Previous studies have identified the importance of symmetry in achieving a high-energy state at the implosion focus, but the exact mechanism preventing symmetry remains unclear. Researchers such as Perry and Kantrowitz, Saito and Glass, and Lee and Knystautas have studied different aspects of imploding shock and detonation waves and found that it is non-trivial to produce symmetric imploding detonations. These studies have explored the fundamental aspects of imploding detonations, including their initiation, propagation, and stability.<br> This study uses modern diagnostic techniques to revisit cylindrical imploding detonations in stoichiometric acetylene-oxygen using experimental tests in a facility similar to the one used by Lee et al. in 1965. By varying the test section geometry, controlled asymmetry of the converging front was produced. A Huygens construct-like numerical model using Fay's boundary layer displacement theory was developed to predict the asymmetry of imploding detonations. A good agreement between results and modeling revealed that the velocity deficit due to boundary layer displacement is the primary mechanism that causes asymmetry. Open-shutter techniques were also used to visualize the cellular structure of converging detonations under varying conditions.";
var next_talk_bio = "Sebastian Rodriguez Rosero is an undergraduate student in honours mechanical engineering at McGill University. As a research assistant at the Thermodynamics and Combustion group, Sebastian has conducted research on gaseous detonations using high-speed optical diagnostics and numerical modeling techniques, focusing on the behavior of imploding detonations under various conditions. In addition to this, Sebastian was also involved in a ground-breaking study that investigated laser thermal propulsion. In this study, he focused on the aerothermodynamics analysis of a Mars aerocapture maneuver and optimizing the astrodynamics requirements for interplanetary missions within the solar system. Currently, Sebastian is a member of the cryogenics team at Anyon Systems, a quantum computing firm. In this role, he is exploring the limitations of thermometry at ultra-low temperatures.";

